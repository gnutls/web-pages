#include 'common.wml' page="Download"

<p>
<center>
  <table class="transparent" border=0 width=80%>
    <tr><td>
         Required libraries:
         <ul>
           <li><a href="https://www.lysator.liu.se/~nisse/nettle/">libnettle</a> crypto back-end</li>
           <li><a href="https://gmplib.org/">gmplib</a> arithmetic library<sup><a href="#fn1" id="ref1">1</a></sup></li>

         </td><td>
        Optional libraries:
        <ul>
          <li><a href="https://www.gnu.org/software/libtasn1/">libtasn1</a> ASN.1 parsing - a copy is included in GnuTLS</li>
          <li><a href="https://p11-glue.freedesktop.org/p11-kit.html">p11-kit</a> for PKCS #11 support</li>
          <li><a href="http://trousers.sourceforge.net/">trousers</a> for TPM support</li>
          <li><a href="https://www.gnu.org/software/libidn/#libidn2">libidn2</a> for Internationalized Domain Names support</li>
          <li><a href="https://unbound.net/">libunbound</a> for DNSSEC/DANE functionality</li>
        </ul>
    </td></tr>
  </table>
</center>
</p>

<perl>
sub print_ver {
my $name = $_[0];
my $abi = $_[1];
my $version = $_[2];

if ($version ne '') {
print " <tr>
    <td>$name</td><td>${version}.x</td>
    <td>$abi</td>
    <td><a href=\"https://www.gnupg.org/ftp/gcrypt/gnutls/v${version}\">
	https://www.gnupg.org/ftp/gcrypt/gnutls/v${version}</a><br>
        <a href=\"ftp://ftp.gnutls.org/gcrypt/gnutls/v${version}\">
	ftp://ftp.gnutls.org/gcrypt/gnutls/v${version}</a> (<a href=\"https://www.gnupg.org/download/mirrors.en.html\">mirror list</a>)</td>
  </tr>\n" if ("${version}" ne "");
}
return;
}

</perl>


<p>

<center>

<h1>Downloading the GnuTLS library</h1>

  All the new releases are signed with at least one OpenPGP key of the <a href="$(path)contrib.html">current maintainers</a>, optionally with the keys of release managers. Those keys are found in the <a href="gnutls-release-keyring.gpg">keyring</a> file.

<table class="news" border=0 cellspacing=1 width=80%>

  <tr>
    <th>Release</th>
    <th>Version</th>
    <th>ABI</th>
    <th>Location</th>
  </tr>


<:= &print_ver("Next<sup><a href=\"#fn2\" id=\"ref2\">2</a></sup>", "$(STABLE_NEXT_ABI)", "$(STABLE_NEXT_VER)") :>
<:= &print_ver("Current stable", "$(STABLE_ABI)", "$(STABLE_VER)") :>

</table>

</p>

<p>
<h1>GnuTLS for Windows</h1>
<p>
The windows builds are automatically generated on every release tag in the
GnuTLS repository. As we do not rely on trusted infrastructure for our CI,
please consider them as untrusted binaries.
</p>
<table class="news" border=0 cellspacing=1 width=80%>

  <tr>
    <th>Description</th>
    <th>Location</th>
  </tr>

  <tr>
    <td>Latest w32 version on gitlab</td>
    <td>
        <a href="https://www.gnupg.org/ftp/gcrypt/gnutls/v$(STABLE_NEXT_VER)/gnutls-$(WINDOWS_TAG)-w32.zip">$(WINDOWS_TAG):mingw32</a>
    </td>
  </tr>

  <tr>
    <td>Latest w64 version on gitlab</td>
    <td>
        <a href="https://www.gnupg.org/ftp/gcrypt/gnutls/v$(STABLE_NEXT_VER)/gnutls-$(WINDOWS_TAG)-w64.zip">$(WINDOWS_TAG):mingw64</a>
    </td>
  </tr>

</table>
</p>
<p>
<h1>GnuTLS in other languages than C</h1>

<table class="news" border=0 cellspacing=1 width=80%>

  <tr>
    <th>Language</th>
    <th>Location</th>
  </tr>
  <tr>
    <td>C++</td>
    <td><a href="https://www.libcxx.org">LibCXX</a><br>
         The GnuTLS distribution also includes a (limited) C++ interface.
    </td>
  </tr>
  <tr>
    <td>Python</td>
    <td><a href="https://pypi.python.org/pypi/python-gnutls/">python-gnutls</a></td>
  </tr>
  <tr>
    <td>PHP</td>
    <td><a href="https://github.com/netaware/php5-gnutls">PHP5-gnutls</a></td>
  </tr>
  <tr>
    <td>Guile</td>
    <td><a href="https://gitlab.com/gnutls/guile">guile-gnutls</a></td>
  </tr>

</table>
</center>
</p>


<p>
<sup id="fn1">1. Gmplib 6 is under LGPLv3 or GPLv2. <a href="ftp://ftp.gmplib.org/pub/gmp/gmp-4.2.1.tar.bz2">Older versions of gmplib</a> under LGPLv2 are also supported.</sup>
<br>
<sup id="fn2">2. <b>Next</b> will be the next stable release; while it is believed to be sufficiently stable it is not as well tested as the stable branch.</sup>
</p>

#include 'bottom.wml'
